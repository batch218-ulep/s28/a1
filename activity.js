// 3. Single Room (insertOne)

db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false
});


// 4. Multiple Rooms (insertMany)

db.rooms.insertMany([
	{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	}
]);

// Updated Queen room:
db.rooms.updateOne(
	{name: "queen"},
	{
		$set: 
		{
		roomsAvailable: 0
		}
	}
);

// DELETE ROOM/S WITH 0 AVAILABILITY
db.rooms.deleteMany({
	roomsAvailable: 0
});

/*FOR CHECKING ---> db.rooms.find();

